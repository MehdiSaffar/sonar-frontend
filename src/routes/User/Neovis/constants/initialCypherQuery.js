export default `MATCH (a:Article) WHERE (a)<-[:IS_TOPIC_OF]-(:Topic { title: 'test' })
MATCH (u:Author)-[aRel:AUTHORED]->(a)
MATCH (aff:Affiliation)<-[affRel:AFFILIATED_WITH]-(u)
MATCH (city:City)<-[cRel:IS_IN_CITY]-(aff)
MATCH (country:Country)<-[conRel:IS_CITY_OF]-(city)
RETURN a, u, aRel, aff, affRel, city, cRel, country, conRel;
`
